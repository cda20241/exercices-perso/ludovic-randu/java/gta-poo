package org.ludo;

import java.util.Scanner;

import static java.lang.System.getProperty;
import static java.lang.System.in;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main extends Fonctions{


    public static final String ALALIGNE = getProperty("line.separator");

    public static void main(String[] args) {
        System.out.println(ALALIGNE);
        System.out.println("Bienvenue dans GTA POO !");
        System.out.println(ALALIGNE);
    // Récupération du pseudo
        Scanner scanner = new Scanner(in);
        System.out.println("Veuillez entrer votre pseudo:");
        String pseudo = scanner.nextLine();
        clearScreen();
    //Vérification si le pseudo est enregistré ou pas
        Personnage joueur1 = new Personnage(pseudo);
        // Si le pseudo n'existe pas, on demande les informations pour l'enregistrer
        if(!joueur1.verificationPseudo(pseudo)){
            // on lance le menu de création du personnage
            System.out.println("Bienvenue "+pseudo+", nous allons créer votre personnage");
            System.out.println(ALALIGNE);
            // Récupération du sexe (1 pour masculin, 2 pour féminin)
            System.out.println("Tapez 1  pour créer un personnage masculin");
            System.out.println("Tapez 2  pour créer un personnage féminin");
            int sexe = scanner.nextInt();
            clearScreen();
            // Récupération de la taille du personnage (Entre 1.20 & 2.10 mètres)
//***************************************************************************** Vérification à faire
            System.out.println("Veuillez entrer la taille de votre personnage (entre 120 et 210cm)");
            int taille = scanner.nextInt();
            clearScreen();
            // Récupération de la longueur des cheveux
//***************************************************************************** Vérification à faire
            System.out.println("Veuillez entrer la longueur de cheveux (entre 0 et 100)");
            int longueurCheveux = scanner.nextInt();
            clearScreen();

//****************Enregistrement du joueur dans fichier "joueurs.json"
            enregistrementNouveauPersonnage(pseudo, sexe, taille,longueurCheveux,1,0);

        }

        // Récupération des informations du personnage dans le fichier joueurs.json
        joueur1.verificationPseudo(pseudo);

        System.out.println("Bonjour " + joueur1.getPseudo().toUpperCase());









    }
}