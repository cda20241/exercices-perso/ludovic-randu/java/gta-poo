package org.ludo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static org.ludo.Main.ALALIGNE;

public class Fonctions {
    private static final String JSON = "./joueurs.json";

    public static void clearScreen() {
        //Fonction qui efface le contenu de la console
        for (int i = 0; i < 50; i++) {
            System.out.print(ALALIGNE);
            System.out.flush();
        }
    }



    public static void enregistrementNouveauPersonnage(String pseudo, int sexe, int taille, int longueurCheveux, int vetement, int extension){
        // Enregistrement du nouveau joueur dans le fichier joueurs.json
        String newEntryJson = "{\n" +
                "    \"pseudo\": \""+pseudo+"\",\n" +
                "    \"sexe\": \""+sexe+"\",\n" +
                "    \"taille\": \""+taille+"\",\n" +
                "    \"longueurCheveux\": \""+longueurCheveux+"\",\n" +
                "    \"vetement\": \""+vetement+"\",\n" +
                "    \"extension\": \""+extension+"\"\n" +
                "}";
        try {
            // Lire le contenu JSON existant depuis le fichier
            String jsonContent = Files.readString(Paths.get(JSON));
            // Créer un objet Gson
            Gson gson = new Gson();
            // Analyser le contenu JSON en une structure de données
            JsonObject jsonObject = gson.fromJson(jsonContent, JsonObject.class);
            // Accéder au tableau "scores" dans l'objet JSON
            JsonArray joueurArray = jsonObject.getAsJsonArray("joueurs");
            // Créer un objet JSON pour la nouvelle entrée
            JsonObject newEntry = gson.fromJson(newEntryJson, JsonObject.class);
            // Ajouter la nouvelle entrée au tableau "scores"
            joueurArray.add(newEntry);
            // Écrire la structure de données mise à jour dans le fichier JSON
            String updatedJson = gson.toJson(jsonObject);
            Files.write(Paths.get(JSON), updatedJson.getBytes(), StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

            // Affichez les données de chaque score

}
