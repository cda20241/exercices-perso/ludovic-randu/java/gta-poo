package org.ludo;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;


public class Personnage extends Joueur {
    // Déclaration des variables du personnage;
    private String pseudo;
    private int sexe;
    private long taille;
    private int longueurCheveux;
    private int vetement;
    private int extension;
// GETTER
    public String getPseudo() {
        return pseudo;
    }
    private static final String JSON = "./joueurs.json";
// CONSTRUCTEUR
    public Personnage(String pseudo) {
        this.pseudo = pseudo;
    }

    public boolean verificationPseudo(String pseudo) {
        /**
         * On verifie dans le fichier joueurs.json si le pseudo existe
         * Return: true si le pseudo existe, false sinon
         */
        String jsonContent = null;
        try {
            jsonContent = Files.readString(Paths.get(JSON));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        Gson gson = new Gson();
        // Analysez le contenu JSON en un objet Java
        JsonObject jsonObject = gson.fromJson(jsonContent, JsonObject.class);
        // Accédez aux données du JSON
        JsonArray scoresArray = jsonObject.getAsJsonArray("joueurs");
        int totalEntries = scoresArray.size();
        int startIndex = Math.max(0, totalEntries - 10);
        boolean pseudoExistant = false;
        for (int i = startIndex; i < totalEntries; i++) {
            JsonObject scoreObject = scoresArray.get(i).getAsJsonObject();
            String verifPseudo = scoreObject.get("pseudo").getAsString();
            if (Objects.equals(verifPseudo, pseudo.toLowerCase())) {
                this.sexe=scoreObject.get("sexe").getAsInt();
                this.taille = scoreObject.get("taille").getAsInt();
                this.longueurCheveux = scoreObject.get("longueurCheveux").getAsInt();
                this.vetement = scoreObject.get("vetement").getAsInt();
                this.extension = scoreObject.get("extension").getAsInt();
                // chargement de la classe personnage avec les données du JSON
                pseudoExistant = true;
            }
        }
        return pseudoExistant;
    }


}
